extends Control

var error
var hearts = 4 setget set_hearts
var max_hearts = 4 setget set_maxhearts

onready var heartUIempty = $HeartUIEmpty
onready var heartUIfull  = $HeartUIFull

func set_hearts(value):
	hearts = clamp(value, 0, max_hearts)
	if heartUIfull != null:
		heartUIfull.rect_size.x = hearts * 15
	
func set_maxhearts(value):
	max_hearts = max(value, 1)
	self.hearts = min(hearts, max_hearts)
	if heartUIempty != null:
		heartUIempty.rect_size.x = max_hearts * 15

func _ready():
	self.max_hearts = PlayerStats.max_health
	self.hearts = PlayerStats.health
	error = PlayerStats.connect("health_changed", self, "set_hearts")
	error = PlayerStats.connect("max_health_changed", self, "set_maxhearts")	
