extends KinematicBody2D

const ACCELERATION = 300
const FRICTION = 200
const MAX_SPEED = 50
export var TARGET_RANGE = 4
const EnemyDeathEffect = preload("res://Effects/EnemyDeathEffect.tscn")

enum {
	IDLE,
	WONDER,
	CHASE
}

var velocity = Vector2.ZERO
var knockback = Vector2.ZERO

onready var state = IDLE

onready var stats = $Stats
onready var sprite = $AnimatedSprite
onready var playerDetectionZone = $PlayerDetectionZone
onready var hurtbox = $HurtBox
onready var softcollision = $SoftCollision
onready var wonderController = $WanderController
onready var animationPlayer = $AnimationPlayer

func _ready():
	state = pick_random_state([IDLE, WONDER])

func _physics_process(delta):
	knockback = knockback.move_toward(Vector2.ZERO, FRICTION*delta)
	knockback = move_and_slide(knockback)
	
	match state:
		IDLE:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION*delta)
			seek_player()
			if wonderController.get_time_left() == 0:
				wonderController.start_wonder_timer(rand_range(3, 5))
				state = pick_random_state([IDLE, WONDER])
		WONDER:
			seek_player()
			if wonderController.get_time_left() == 0:
				wonderController.start_wonder_timer(rand_range(3, 5))
				state = pick_random_state([IDLE, WONDER])

			accelerate_towards_point(wonderController.target_position, delta)
			
			if int(global_position.distance_to(wonderController.target_position)) == 0:
				state = pick_random_state([IDLE, WONDER])
		CHASE:
			var player = playerDetectionZone.player
			if player != null:
				accelerate_towards_point(player.global_position, delta)
			else:
				state = IDLE
	if softcollision.is_colliding():
		velocity += softcollision.get_push_vectors() * delta * ACCELERATION
	velocity = move_and_slide(velocity)
	
func accelerate_towards_point(point, delta):
	var direction = global_position.direction_to(point)
	velocity = velocity.move_toward(direction * MAX_SPEED, ACCELERATION*delta)
	sprite.flip_h = velocity.x < 0
	
func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front()

func seek_player():
	if playerDetectionZone.can_see_player():
		state = CHASE

func _on_HurtBox_area_entered(area):
	stats.health -= area.damage
	knockback = area.knockback_vector * 120
	hurtbox.start_invincibility(0.4)
	hurtbox.create_hit_effect()

func _on_Stats_no_health():
	queue_free()
	var enemy_death_effect = EnemyDeathEffect.instance()
	get_parent().add_child(enemy_death_effect)
	enemy_death_effect.global_position = global_position

func _on_HurtBox_invincibility_started():
	animationPlayer.play("Start")

func _on_HurtBox_invincibility_ended():
	animationPlayer.play("Stop")
