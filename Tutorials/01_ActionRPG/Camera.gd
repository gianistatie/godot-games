extends Camera2D

onready var topleft = $Node/TopLeft
onready var bottomright = $Node/BottomRight

# Called when the node enters the scene tree for the first time.
func _ready():
	limit_bottom = bottomright.position.y
	limit_left = topleft.position.x
	limit_top = topleft.position.y
	limit_right = bottomright.position.x
